#!/usr/bin/env python
# Copyright 2018 Chad Hanna
#
import sys
import os
import subprocess

def process_source(prog, outfile):
	with open(prog, 'r') as fid:
		for line in fid.readlines():
			if not line.startswith("###"):
				continue
			outfile.write(line.replace("### ", "").replace("###",""))
		

if len(sys.argv) == 1:
	print("USAGE: sphinx-bindoc <output directory> <input directory> [patterns to exclude]")
	sys.exit()

assert(len(sys.argv) >= 3)

indir = sys.argv[2]
outdir = sys.argv[1]

tocf = open(os.path.join(outdir, "bin.rst"), "w")
tocf.write("""bin
=====================

.. toctree::
   :maxdepth: 1
""")

for prog in sorted(os.listdir(indir)):
	# Don't document make files
	if "Makefile" in prog:
		continue
	if prog in sys.argv[3:]:
		continue

	path_to_prog = os.path.join(indir, prog)

	# Write the rst file that contains command line arguments
	fname = os.path.join(outdir, prog+".rst")

	# register this program in the master list of programs
	tocf.write("\n   %s" % os.path.split(fname)[-1].replace(".rst",""))

	if os.path.exists(fname):
		print("File %s already exists, skipping." % fname)
		continue
	else:
		print("Creating file ", fname)

	with open(fname, "w") as f:
		# parse the bin program itself for additional documentation
		f.write("%s\n%s\n\n" % (prog, "".join(["="] * len(prog))))
		process_source(path_to_prog, f)

		# write the output of --help
		f.write("%s\n%s\n\n" % ("Command line options", "".join(["-"] * len("Command line options"))))
		f.write("\n\n.. code-block:: none\n\n")
		try:
			proc = subprocess.Popen([path_to_prog, "--help"], stdout = subprocess.PIPE)
			helpmessage = proc.stdout.read()
			if isinstance(helpmessage, bytes):
				helpmessage = helpmessage.decode('utf-8')
			helpmessage = "\n".join(["   %s" % l for l in helpmessage.split('\n')])
			f.write(helpmessage)
		except OSError:
			pass

tocf.close()
