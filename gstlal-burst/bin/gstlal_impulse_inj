#!/usr/bin/env python3
#
# Copyright (C) 2021  Soichiro Kuwahara
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""Delta function injecting tool"""

#
# =============================================================================
#
#                                   Preamble
#
# =============================================================================
#


import math
from optparse import OptionParser
import sys

import lal

from ligo.lw import ligolw
from ligo.lw import lsctables
from ligo.lw import utils as ligolw_utils
from ligo.lw.utils import process as ligolw_process


@lsctables.use_in
class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
	pass


__author__ = "Soichrio Kuwahara <soichiro.kuwahara@ligo.org>"

program_name = "gstlal_impulse_inj"


#
# =============================================================================
#
#                                 Command Line
#
# =============================================================================
#


def parse_command_line():
	parser = OptionParser(
		description = "GstLAL-based delta function injection pipeline."
	)
	parser.add_option("--hpeak", type = "float", help = "Set beta.")
	parser.add_option("--output", metavar = "filename", help = "Set the name of the output file (default = stdout).")
	parser.add_option("--time-slide-file", metavar = "filename", help = "Associate injections with the first time slide ID in this XML file (required).")
	parser.add_option("--gps-geocent-time", metavar = "s", help = "Set the start time of the tiling in GPS seconds (required).")
	parser.add_option("--verbose", action = "store_true", help = "Be verbose.")
	options, filenames = parser.parse_args()
	# FIXME: Designed only for injecting one impulse.

	# save for the process_params table
	options.options_dict = dict(options.__dict__)

	# check for params
	required_options = set(("hpeak", "output", "time_slide_file", "gps_geocent_time"))
	missing_options = set(option for option in required_options if getattr(options, option) is None)
	if missing_options:
		raise ValueError("missing required option(s) %s" % ", ".join("--%s" % options.subst("_", "_") for option in (requied_options - missing_options)))

	# type-cast
	options.gps_geocent_time = lal.LIGOTimeGPS(options.gps_geocent_time)

	return options, filenames


#
# =============================================================================
#
#                                     Main
#
# =============================================================================
#


options, filenames = parse_command_line()


#
# use the time-slide file to start the output document
#


xmldoc = ligolw_utils.load_filename(options.time_slide_file, verbose = options.verbose, contenthandler = LIGOLWContentHandler)


#
# add our metadata
#


process = ligolw_process.register_to_xmldoc(xmldoc, "impulse_inj", options.options_dict)


#
# use whatever time slide vector comes first in the table (lazy)
#


time_slide_table = lsctables.TimeSlideTable.get_table(xmldoc)
time_slide_id = time_slide_table[0].time_slide_id
if options.verbose:
	print("associating injections with time slide (%d) %s" % (time_slide_id, time_slide_table.as_dict()[time_slide_id]), file = sys.stderr)


#
# find or add a sim_burst table
#


try:
	lsctables.SimBurstTable.get_table(xmldoc)
except ValueError:
	# no sim_burst table in document
	pass
else:
	raise ValueError("%s contains a sim_burst table.  this program isn't smart enough to deal with that." % options.time_slide_xml)

sim_burst_tbl = xmldoc.childNodes[-1].appendChild(lsctables.New(lsctables.SimBurstTable, ["process:process_id", "simulation_id", "time_slide:time_slide_id", "waveform", "waveform_number", "ra", "dec", "psi", "q", "hrss", "time_geocent_gps", "time_geocent_gps_ns", "time_geocent_gmst", "duration", "frequency", "bandwidth", "egw_over_rsquared", "amplitude", "pol_ellipse_angle", "pol_ellipse_e"]))


#
# populate the sim_burst table with injections
#


sim_burst_tbl.append(sim_burst_tbl.RowType(
	# metadata
	process_id = process.process_id,
	simulation_id = sim_burst_tbl.get_next_id(),
	time_slide_id = time_slide_id,
	waveform = "Impulse",

	# waveform parameters
	time_geocent = options.gps_geocent_time,
	amplitude = options.hpeak,

	# FIXME.  sky location and polarization axis orientation
	ra = 0.,
	dec = 0.,
	psi = 0.,

	# unnecessary columns
	waveform_number = 0.,
	frequency = math.nan,
	bandwidth = math.nan,
	egw_over_rsquared = math.nan,
	duration = math.nan,
	q = math.nan,
	hrss = math.nan,
	pol_ellipse_angle = math.nan,
	pol_ellipse_e = math.nan
))


#
# write output
#


ligolw_utils.write_filename(xmldoc, options.output, verbose = options.verbose)
