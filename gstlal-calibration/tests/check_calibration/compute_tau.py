#!/usr/bin/env python3
# Copyright (C) 2021  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import numpy as np
import os
from optparse import OptionParser, Option
import configparser


parser = OptionParser()
parser.add_option("--kappar-txt", metavar = "name", help = "Name of txt file with a time series of real parts of kappa")
parser.add_option("--kappai-txt", metavar = "name", help = "Name of txt file with a time series of imaginary parts of kappa")
parser.add_option("--config-file", metavar = "name", help = "Name of config file used to produce the TDCF data")
parser.add_option("--kappa-filename", metavar = "name", type = str, help = "Name of output file with magnitude of TDCF")
parser.add_option("--tau-filename", metavar = "name", type = str, help = "Name of output file with tau, the time advance associated with the TDCF")

options, filenames = parser.parse_args()

kr = np.loadtxt(options.kappar_txt)
ki = np.loadtxt(options.kappai_txt)
t = np.transpose(kr)[0]
kr = np.transpose(kr)[1]
ki = np.transpose(ki)[1]

kappa = np.zeros(len(t))
tau = np.zeros(len(t))


# Find the frequency of the actuator line used to measure this TDCF.
# First, we need the config file.
def ConfigSectionMap(section):
	dict1 = {}
	options = Config.options(section)
	for option in options:
		try:
			dict1[option] = Config.get(section, option)
			if dict1[option] == -1:
				DebugPrint("skip: %s" % option)
		except:
			print("exception on %s!" % option)
			dict[option] = None
	return dict1

Config = configparser.ConfigParser()
Config.read(options.config_file)
InputConfigs = ConfigSectionMap("InputConfigurations")

# Next, we need the filters file.
# Search the directory tree for files with names matching the one we want.
filters_name = InputConfigs["filtersfilename"]
if filters_name.count('/') > 0:
	# Then the path to the filters file was given
	filters = np.load(filters_name)
else:
	# We need to search for the filters file
	filters_paths = []
	print("\nSearching for %s ..." % filters_name)
	# Check the user's home directory
	for dirpath, dirs, files in os.walk(os.environ['HOME']):
		if filters_name in files:
			# We prefer filters that came directly from a GDSFilters directory of the calibration SVN
			if dirpath.count("GDSFilters") > 0:
				filters_paths.insert(0, os.path.join(dirpath, filters_name))
			else:
				filters_paths.append(os.path.join(dirpath, filters_name))
	# Check if there is a checkout of the entire calibration SVN
	for dirpath, dirs, files in os.walk('/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/'):
		if filters_name in files:
			# We prefer filters that came directly from a GDSFilters directory of the calibration SVN
			if dirpath.count("GDSFilters") > 0:
				filters_paths.insert(0, os.path.join(dirpath, filters_name))
			else:
				filters_paths.append(os.path.join(dirpath, filters_name))
	if not len(filters_paths):
		raise ValueError("Cannot find filters file %s in home directory %s or in /ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/*/GDSFilters", (filters_name, os.environ['HOME']))
	print("Loading calibration filters from %s\n" % filters_paths[0])
	filters = np.load(filters_paths[0])

# Next, we need to infer what TDCF we are dealing with to choose the line.
if 'TST' in options.kappar_txt or 'tst' in options.kappar_txt:
	freq = filters['ktst_esd_line_freq']
elif 'PUM' in options.kappar_txt or 'pum' in options.kappar_txt:
	freq = filters['pum_act_line_freq']
else:
	freq = filters['uim_act_line_freq']


for i in range(len(t)):
	kappa[i] = abs(kr[i] + 1j * ki[i])
	tau[i] = np.angle(kr[i] + 1j * ki[i]) / 2 / np.pi / freq * 1000000.0

np.savetxt(options.kappa_filename, np.transpose([t, kappa]), fmt = ['%f', '%8e'], delimiter = "   ")
np.savetxt(options.tau_filename, np.transpose([t, tau]), fmt = ['%f', '%8e'], delimiter = "   ")


