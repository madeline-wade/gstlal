#!/usr/bin/env python3
#
# Copyright (C) 2020  Patrick Godwin (patrick.godwin@ligo.org)
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import argparse

from gstlal.config.inspiral import Config
from gstlal.dags.inspiral import DAG
from gstlal.datafind import DataCache, DataType


parser = argparse.ArgumentParser()
parser.add_argument("-c", "--config", help="Sets the path to read configuration from.")

# load config
args = parser.parse_args()
config = Config.load(args.config)

# create dag
dag = DAG(config)
dag.create_log_dir()

# input data products
split_bank = DataCache.find(DataType.SPLIT_BANK, svd_bins="*", subtype="*")
ref_psd = DataCache.from_files(DataType.REFERENCE_PSD, config.data.reference_psd)

# generate dag layers
svd_bank = dag.svd_bank(ref_psd, split_bank)

# write dag/script to disk
dag_name = "svd_bank_dag"
dag.write_dag(f"{dag_name}.dag")
dag.write_script(f"{dag_name}.sh")
