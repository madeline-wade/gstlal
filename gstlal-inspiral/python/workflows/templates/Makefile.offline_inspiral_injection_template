all : dag

.PHONY: launch
launch : {{ workflow }}_inspiral_dag.dag
	condor_submit_dag $<

.PHONY: dag
dag : {{ workflow }}_inspiral_dag.dag
	@echo ""

.PHONY: summary
summary :
	mkdir -p {{ config.summary.webdir }}
	gstlal_inspiral_summary_page \
		--title gstlal-{{ config.start }}-{{ config.stop }}-closed-box \
		--webserver-dir {{ config.summary.webdir }} \
		--output-user-tag ALL_COMBINED \
		--output-user-tag PRECESSION_COMBINED \
		{% for inj_name in config.filter.injections.keys() %}
		--output-user-tag {{ inj_name.upper() }}_INJECTION \
		--output-user-tag {{ inj_name.upper() }}_INJECTION_PRECESSION \
		{% endfor %}
		--glob-path plots

segments.xml.gz :
	cp {{ config.data.analysis_dir }}/$@ $@
	@echo ""

vetoes.xml.gz :
	cp {{ config.data.analysis_dir }}/$@ $@
	@echo ""

{{ config.svd.manifest }} :
	cp {{ config.data.analysis_dir }}/$@ $@

tisi.xml : inj_tisi.xml
	lalapps_gen_timeslides {% for instrument, slides in config.filter.time_slides.items() %} --instrument={{ instrument }}={{ slides }}{% endfor %} bg_tisi.xml
	ligolw_add --output $@ bg_tisi.xml $<
	@echo ""

inj_tisi.xml :
	lalapps_gen_timeslides {% for instrument in config.ifos %} --instrument={{ instrument }}=0:0:0{% endfor %} $@
	@echo ""

%_inspiral_dag.dag : {{ config.svd.manifest }} vetoes.xml.gz segments.xml.gz tisi.xml plots {% for inj in config.filter.injections.values() %} {{ inj.file }}{% endfor %}

	gstlal_inspiral_workflow create -c config.yml --workflow $*

{% if config.injections.sets %}
{% for inj_name, params in config.injections.sets.items() %}
{{ params.file }} :
	lalapps_inspinj \
		--gps-start-time {{ config.start + params.time.shift }} \
		--gps-end-time {{ config.stop }} \
		--enable-spin \
		--aligned \
		--i-distr uniform \
		--l-distr random \
		--t-distr uniform \
		--dchirp-distr uniform \
		--m-distr {{ params.mass_distr }} \
		{% for param in ['mass1', 'mass2', 'spin1', 'spin2', 'distance'] %}
		{% for stat, val in params[param].items() %}
		--{{ stat }}-{{ param }} {{ val }} \
		{% endfor %}
		{% endfor %}
		--f-lower {{ params.f_low }} \
		--waveform {{ params.waveform }} \
		--time-step {{ params.time.step }} \
		--time-interval {{ params.time.interval }} \
		--taper-injection startend \
		--seed {{ params.seed }} \
		--output $@
	ligolw_no_ilwdchar $@
	@echo ""

{% endfor %}
{% endif %}
{% if config.injections.combine %}
{{ config.injections.combined_file }} : {% for inj in config.injections.sets.values() %} {{ inj.file }}{% endfor %}

	gstlal_inspiral_combine_injection_sets $^ --single-output -o $(basename {{ config.injections.combined_file }})
	rm injection_str.txt
{% endif %}

plots :
	mkdir -p $@

clean : 
	rm -rf segments.xml.gz *tisi.xml
	rm -rf split_bank *vetoes.xml.gz
	rm -rf reference_psd median_psd
	rm -rf filter rank plots
	rm -rf logs *inspiral_dag.dag* *inspiral_dag.sh *.sub _condor_stdout
	rm -rf {% for inj in config.filter.injections.values() %} {{ inj.file }}{% endfor %}

	{% if config.injections.sets %}
	rm -rf {% for inj in config.injections.sets.values() %} {{ inj.file }}{% endfor %}
	{% endif %}


clean-lite :
	rm -rf logs/* *inspiral_dag.dag* *inspiral_dag.sh *.sub
