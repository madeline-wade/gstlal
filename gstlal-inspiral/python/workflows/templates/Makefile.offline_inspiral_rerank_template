all : dag

.PHONY: launch
launch : {{ workflow }}_inspiral_dag.dag
	condor_submit_dag $<

.PHONY: dag
dag : {{ workflow }}_inspiral_dag.dag
	@echo ""

.PHONY: summary
summary :
	mkdir -p {{ config.summary.webdir }}
	gstlal_inspiral_summary_page \
		--title gstlal-{{ config.start }}-{{ config.stop }}-closed-box \
		--webserver-dir {{ config.summary.webdir }} \
		--output-user-tag ALL_COMBINED \
		--output-user-tag PRECESSION_COMBINED \
		{% for inj_name in config.filter.injections.keys() %}
		--output-user-tag {{ inj_name.upper() }}_INJECTION \
		--output-user-tag {{ inj_name.upper() }}_INJECTION_PRECESSION \
		{% endfor %}
		--glob-path plots

segments.xml.gz :
	cp {{ config.data.analysis_dir }}/$@ $@
	@echo ""

vetoes.xml.gz :
	cp {{ config.data.analysis_dir }}/$@ $@
	@echo ""

{{ config.svd.manifest }} :
	cp {{ config.data.analysis_dir }}/$@ $@

%_inspiral_dag.dag : {{ config.svd.manifest }} vetoes.xml.gz segments.xml.gz plots
	gstlal_inspiral_workflow create -c config.yml --workflow $*

plots :
	mkdir -p $@

clean : 
	rm -rf segments.xml.gz *tisi.xml
	rm -rf rank plots
	rm -rf logs *inspiral_dag.dag* *inspiral_dag.sh *.sub _condor_stdout

clean-lite :
	rm -rf logs/* *inspiral_dag.dag* *inspiral_dag.sh *.sub
