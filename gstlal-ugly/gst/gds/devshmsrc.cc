//
// GDS LIGO-Virgo /dev/shm frame file source element
//
// Copyright (C) Ron Tapia, The Pennsylvania State University 2022
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//
// SECTION:devshmsrc
// @short_description: LIGO-Virgo /dev/shm frame file source element.
//
// Reviewed: NOT REVIEWED
//
// Actions:
// - Review
//

//
// C/OS Library includes
//
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <time.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <poll.h>

//
// glib/goobject/gstreamer includes
//
#include <glib.h>
#include <gst/gst.h>
#include <gst/base/gstpushsrc.h>
//
// gds includes
//
#include <gds/lsmp_con.hh>
#include <gds/tconv.h>
#include <gds/SysError.hh>
//
// gstlal includes
//
#include <gstlal/gstlal_debug.h>
#include <devshmsrc.h>

#define URI_SCHEME "devshm"

//
// Declare utility functions
//
static ssize_t r_read (int fd, void *buf, size_t size);
static void free_queue_element (void *data, void *userData);
static GstFlowReturn devshm_fill (GstBaseSrc *basesrc, GstBuffer **bufferpp);
static GstFlowReturn devshm_open_next_file (GstBaseSrc *basesrc);
static GstFlowReturn devshm_get_filenames (GstBaseSrc *basesrc);
static int devshm_timestamp_for_file(const char *filename, GstClockTime *timestamp);

//
//
//
static GstURIType uri_get_type(GType type)
{ 
  return GST_URI_SRC; 
}

static const gchar *const *uri_get_protocols(GType type)
{
  static const gchar *protocols[] = {URI_SCHEME, NULL};
  return protocols;
}

static gchar *uri_get_uri(GstURIHandler *handler)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(handler);
  return g_strdup_printf(URI_SCHEME ":%s", element->name);
}

static gboolean uri_set_uri(GstURIHandler *handler, const gchar *uri, GError **err)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(handler);
  gchar *scheme = g_uri_parse_scheme(uri);
  gchar name[strlen(uri)];
  gboolean success = TRUE;

  success = !strcmp(scheme, URI_SCHEME);
  if(success)
    success &= sscanf(uri, URI_SCHEME ":%s", name) == 1;
  if(success) {
    if (name[strlen(name)] == '/') {
      name[strlen(name)] = 0;
    }
    g_object_set(G_OBJECT(element), "shm-dir", name, NULL);
  }
  g_free(scheme);
  return success;    
}

static void uri_handler_init(gpointer g_iface, gpointer iface_data)
{
  GstURIHandlerInterface *iface = (GstURIHandlerInterface *) g_iface;

  iface->get_uri = GST_DEBUG_FUNCPTR(uri_get_uri);
  iface->set_uri = GST_DEBUG_FUNCPTR(uri_set_uri);
  iface->get_type = GST_DEBUG_FUNCPTR(uri_get_type);
  iface->get_protocols = GST_DEBUG_FUNCPTR(uri_get_protocols);
}

#define GST_CAT_DEFAULT gds_devshmsrc_debug
GST_DEBUG_CATEGORY_STATIC(GST_CAT_DEFAULT);

static void additional_initializations(GType type)
{
  static const GInterfaceInfo uri_handler_info = {
    uri_handler_init,
    NULL,
    NULL
  };
  g_type_add_interface_static(type, GST_TYPE_URI_HANDLER, &uri_handler_info);
  
  GST_DEBUG_CATEGORY_INIT(GST_CAT_DEFAULT, "gds_devshmsrc", 0, "gds_devshmsrc element");
}

G_DEFINE_TYPE_WITH_CODE(GDSDEVSHMSrc,
                        gds_devshmsrc,
                        GST_TYPE_PUSH_SRC,
                        additional_initializations(g_define_type_id));

//
// Paremeters
//
#define DEFAULT_SHM_DIRNAME NULL    
#define DEFAULT_MASK -1             //
#define DEFAULT_WAIT_TIME -1.0      // wait indefinitely
#define DEFAULT_ASSUMED_DURATION 1  //

//
// Utilities
//
static GstClockTime GPSNow(void)
{
  // FIXME:  why does TAInow() return the GPS time? Is this necessary?
  // TAI is "Temps Atomique International" or "international atomic time".
  // http://leapsecond.com/java/gpsclock.htm
  // TAInow is defined in gds/Base/time/tconv.c
  // This return value of TAInow appears to be in nanoseconds.
  // Is gst_util_uint64_scale_int_round necessary?
  // It certainly seems odd that we use a function named TAInow,
  // scale its return value by 1, and return it from a function named GPSNow.
  return gst_util_uint64_scale_int_round(TAInow(), GST_SECOND, _ONESEC);
}

//
// Handle request to start processing.
//
static gboolean start(GstBaseSrc *object)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(object);
  gboolean success = TRUE;
  int status;
  
  element->currentFd = -1;
  element->next_timestamp = 0;

  if(!element->name) {
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("shm-dir not set"));
    success = FALSE;
    goto done;
  }

  element->notifyFd = inotify_init1(IN_NONBLOCK);

  if (element->notifyFd == -1) {
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("could not initialize inotify for shm-dir: \"%s\"", element->name));
    success = FALSE;
    goto done;
  }
  status = inotify_add_watch(element->notifyFd, element->name, IN_ONLYDIR|IN_CLOSE_WRITE);
  if (status == -1) {
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("could not add watch on shm-dir: \"%s\" : %s", element->name, strerror(errno)));
    success = FALSE;
    goto done;
  }
  element->inotifyWatchDescriptor = status;
  GST_DEBUG_OBJECT(element, "watching directory: \"%s\"", element->name);

  // If g_malloc fails, it takes the program down, so no need to check return value.
  element->inotifyEventLen = DEVSHM_MAX_FILE_EVENTS * (sizeof(struct inotify_event) + NAME_MAX + 1);
  element->inotifyEventp = (char *) g_malloc(element->inotifyEventLen);
  GST_DEBUG_OBJECT(element, "allocated memory for inotify events");

  // Can g_queue_new() fail...I mean without taking down the program because of a g_malloc?
  element->filenameQueue = g_queue_new();
  GST_DEBUG_OBJECT(element, "created filenameQueue");

  element->currentFullPath = g_string_sized_new(PATH_MAX + 1);

 done:
  GST_DEBUG_OBJECT(element, "start complete, return value: %d", success);
  return success;
}

//
// Handle request to stop processing.
//
static gboolean stop(GstBaseSrc *object)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(object);

  close(element->notifyFd);
  g_free(element->inotifyEventp);
  g_queue_foreach(element->filenameQueue, &free_queue_element, NULL);
  g_queue_free(element->filenameQueue);
  g_string_free(element->currentFullPath, TRUE);
    
  return TRUE;
}

static gboolean unlock(GstBaseSrc *basesrc)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(basesrc);
  gboolean success = TRUE;

  element->unblocked = TRUE;
  if(!g_mutex_trylock(&element->create_thread_lock))
    success = !pthread_kill(element->create_thread, SIGALRM);
  else
    g_mutex_unlock(&element->create_thread_lock);

  return success;
}

static gboolean unlock_stop(GstBaseSrc *basesrc)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(basesrc);
  gboolean success = TRUE;

  element->unblocked = FALSE;

  return success;
}

//
// Handle a request to create a buffer. Ignore offset and size.
//
static GstFlowReturn create(GstBaseSrc *basesrc, guint64 offset, guint size, GstBuffer **bufferpp)
{
  GDSDEVSHMSrc  *element = GDS_DEVSHMSRC(basesrc);
  GstFlowReturn  result  = GST_FLOW_OK; 

  *bufferpp = NULL;

  // Can create be called in multiple threads concurrently?
  // If so, these locks are necessary. If not, they
  // are, like the inhabitants of earth, mostly harmless.
  element->create_thread = pthread_self();
  g_mutex_lock(&element->create_thread_lock);
  result = devshm_fill(basesrc, bufferpp);
  g_mutex_unlock(&element->create_thread_lock);

  return result;    
}

//
// Given a pointer to a pointer to a GstBuffer, create a GstBuffer (sized appropriately)
// that holds the contents of the next frame file in the queue.
//
static GstFlowReturn devshm_fill (GstBaseSrc *basesrc, GstBuffer **bufferpp)
{
  GstBaseSrcClass *basesrc_class = GST_BASE_SRC_CLASS(G_OBJECT_GET_CLASS(basesrc));
  GDSDEVSHMSrc    *element = GDS_DEVSHMSRC(basesrc);
  GstFlowReturn    result = GST_FLOW_OK;
  GstMapInfo       mapinfo;
  size_t           bytesRead = 0;
  ssize_t          readResult;

  result = devshm_open_next_file(basesrc);
  if (result != GST_FLOW_OK) {
    goto done;
  }

  result = basesrc_class->alloc(basesrc, 0, element->currentFileSize, bufferpp);
  if(result != GST_FLOW_OK) {
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("alloc() returned %d (%s)", result, gst_flow_get_name(result)));
    goto done;
  }

  gst_buffer_map(*bufferpp, &mapinfo, GST_MAP_WRITE);

  do {
    readResult = r_read(element->currentFd, mapinfo.data + bytesRead, mapinfo.size - bytesRead);
    if (readResult < 0) {
      result = GST_FLOW_ERROR;
      GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("read('%s') failed: %s", element->currentFilename, strerror(errno)));
      gst_buffer_unmap(*bufferpp, &mapinfo);
      gst_buffer_unref(*bufferpp);
      *bufferpp = NULL;
      goto done;
    } else {
      bytesRead += readResult;
    }
  } while (bytesRead < element->currentFileSize);

  g_assert_cmpuint(bytesRead, ==, element->currentFileSize);

  GST_BUFFER_PTS(*bufferpp)      = element->currentFileTimestamp * GST_SECOND;
  GST_BUFFER_DURATION(*bufferpp) = GST_SECOND;
  element->next_timestamp        = GST_BUFFER_PTS(*bufferpp) + GST_BUFFER_DURATION(*bufferpp);

  element->max_latency    = GPSNow() - GST_BUFFER_PTS(*bufferpp);
  element->min_latency    = element->max_latency - GST_BUFFER_DURATION(*bufferpp);    
  GST_DEBUG_OBJECT(element, "max_latency: %ld min_latency: %ld  shm-dirname: \"%s\"", element->max_latency, element->min_latency, element->name);

  close(element->currentFd);
  element->currentFd = -1;
  gst_buffer_unmap(*bufferpp, &mapinfo);

 done:
  return result;
}

//
// Open the next filename on the filename queue. 
// If the filename queue is empty, fill it.
//
static GstFlowReturn devshm_open_next_file (GstBaseSrc *basesrc) 
{
  GDSDEVSHMSrc  *element = GDS_DEVSHMSRC(basesrc);
  GstFlowReturn  result  = GST_FLOW_OK;
  GString       *nextFilenameGstringp;
  int            status;
  struct stat    statinfo;

  if (g_queue_get_length(element->filenameQueue) == 0) {
    result = devshm_get_filenames(basesrc);
    if (result != GST_FLOW_OK) {
      GST_DEBUG_OBJECT(element, "devshm_get_filenames returned error: \"%s\"", element->name);      
      goto done;
    }
  }

  // Pop item off the queue and free as soon as we're done using it.
  nextFilenameGstringp = (GString *) g_queue_pop_head(element->filenameQueue);
  strncpy(element->currentFilename, nextFilenameGstringp->str, nextFilenameGstringp->len + 1);
  GST_DEBUG_OBJECT(element, "new filename: \"%s\"", element->currentFilename);      
  g_string_free(nextFilenameGstringp, TRUE);

  if (devshm_timestamp_for_file(element->currentFilename, &(element->currentFileTimestamp)) != 0) {
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("no timestamp in filename: '%s'", element->currentFilename));
    result = GST_FLOW_ERROR;
    goto done;
  }

  g_string_printf(element->currentFullPath, "%s/%s", element->name, element->currentFilename);
  element->currentFd = open(element->currentFullPath->str, O_RDONLY);

  // Should an error opening the next file be a GST_FLOW_ERROR?
  // Maybe we are running behind and the file was deleted.
  // That would put us at 5 minutes behind which is worrisome.
  // For now, consider it an error.
  if (element->currentFd == -1) {
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("failed to open file: '%s' : %s", element->currentFilename, strerror(errno)));
    result = GST_FLOW_ERROR;
    goto done;
  }
  GST_DEBUG_OBJECT(element, "opened file: \"%s\"", element->currentFullPath->str);

  // Find the size of the current file.
  status = fstat(element->currentFd, &statinfo);
  if (status != 0) {
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("fstat('%s') failed: %s", element->currentFilename, strerror(errno)));
    result = GST_FLOW_ERROR;
    close(element->currentFd);
    goto done;
  }
  element->currentFileSize = statinfo.st_size;
  GST_DEBUG_OBJECT(element, "size of file: \"%s\" is %ld bytes", element->currentFilename, statinfo.st_size);  

 done:
  return result;
}

//
// Poll the notification fd. If we timeout, it is an error:
// no new files are being written to /dev/shm. It is best
// to die and hope that we run again on a new, healthy, server.
//
static GstFlowReturn devshm_get_filenames (GstBaseSrc *basesrc)
{
  GDSDEVSHMSrc  *element = GDS_DEVSHMSRC(basesrc);
  GstFlowReturn  result  = GST_FLOW_OK;
  ssize_t        readResult = 0;
  size_t         parsedEventsLen = 0;
  GString       *filenameGstringp;
  int            count = 0;
  struct pollfd  pfd;
  clock_t        startTime, elapsed;
  struct inotify_event *eventp;
  
  // Poll the notify file descriptor.
  // The timeout should be several minutes. If we are not seeing files,
  // something is wrong. 
  pfd.fd     = element->notifyFd;
  pfd.events = POLLIN;
  startTime = clock();
  poll(&pfd, 1, DEVSHM_NEW_FILE_TIMEOUT);
  elapsed = clock() - startTime;

  if (pfd.revents & POLLIN) {
    readResult = r_read(element->notifyFd, element->inotifyEventp, element->inotifyEventLen);
  } else {
    //timeout or some error that we don't know how to handle
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("after %f seconds, timeout or error polling notification fd for dir \"%s\": %s", (float)elapsed/CLOCKS_PER_SEC, element->name, strerror(errno)));    
    result = GST_FLOW_ERROR;
    goto done;
  }

  if (readResult == -1) {
    if (errno == EAGAIN) {
      GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("read returned EAGAIN after successful poll (very odd): %s", strerror(errno)));
      result = GST_FLOW_ERROR;
      goto done;
    }
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("failed to read notifyFd: %s", strerror(errno)));
    result = GST_FLOW_ERROR;
    goto done;
  }

  GST_DEBUG_OBJECT(element, "read %ld bytes from inotify fd: \"%s\"", readResult, element->name);

  eventp = (struct inotify_event *) element->inotifyEventp;
  while (parsedEventsLen < (size_t) readResult) {
    GST_DEBUG_OBJECT(element, "parsing inotify event %d (size: %ld + %d) in \"%s\"", count, sizeof(struct inotify_event), eventp->len, element->name);
    g_assert_cmpint(eventp->wd, ==, element->inotifyWatchDescriptor);
    if (eventp->len == 0) {
      GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("zero length filename in file create notification event"));
      result = GST_FLOW_ERROR;
      goto done;
    } else if (eventp->len > NAME_MAX) {
      GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("filename longer than NAME_MAX in file create notification event"));      
      result = GST_FLOW_ERROR;
      goto done;      
    }
    filenameGstringp = g_string_new(eventp->name);
    g_queue_push_tail(element->filenameQueue, (gpointer) filenameGstringp);
    parsedEventsLen += sizeof(struct inotify_event) + eventp->len;
    eventp = (struct inotify_event *) (element->inotifyEventp + parsedEventsLen);
    count++;
  }

  GST_DEBUG_OBJECT(element, "found %d new files in \"%s\"", count, element->name);
  
 done:
  return result;
}

static gboolean query(GstBaseSrc *basesrc, GstQuery *query)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(basesrc);
  gboolean success = TRUE;  
  switch(GST_QUERY_TYPE(query)) {
  case GST_QUERY_FORMATS:
    gst_query_set_formats(query, 1, GST_FORMAT_TIME);
    break;
  case GST_QUERY_LATENCY:
    gst_query_set_latency(query, gst_base_src_is_live(basesrc), element->min_latency, element->max_latency);
    break;
  case GST_QUERY_POSITION:
    // timestamp of next buffer
    gst_query_set_position(query, GST_FORMAT_TIME, element->next_timestamp);
    break;
  default:
    success = GST_BASE_SRC_CLASS(gds_devshmsrc_parent_class)->query(basesrc, query);
    break;
  }
  if(success)
    GST_DEBUG_OBJECT(element, "query result: %" GST_PTR_FORMAT, query);
  else
    GST_ERROR_OBJECT(element, "query failed");
  
  return success;
}

//
// Version of read that handles being interrupted by a signal.
//
static ssize_t r_read (int fd, void *buf, size_t size)
{
  ssize_t retval;
  while (retval = read(fd, buf, size), retval == -1 && errno == EINTR) ;
  return retval;
}

//
// Utility function for freeing our filename queue elements (which are all GStrings).
//
static void free_queue_element (void *data, void *userData) 
{
  g_string_free((GString *) data, TRUE);
}

//
// Find the timestamp in a frame format filename. The 
// format of the filename is expected to be:
//
//    FOO-FOO-TIMESTAMP-1.gwf
//
// The timestamp returned is in GPS seconds.
//
// Note: Based on: https://dcc.ligo.org/LIGO-T010150
//       the last number of the file name is not necessarily the duration
//       of the frame data in the file. It works out this way for
//       the file processed out of /dev/shm, but it need not be 
//       the case in general.
//
static int devshm_timestamp_for_file(const char *filename, GstClockTime *timestamp) 
{
  const char *ts_start = 0;
  uint  i;
  uint  count = 0;
  long  retval = 0;

  for (i=0; i< strlen(filename); i++) {
    if (*(filename + i) == '-') {
      count++;
    }
    if (count == 2) {
      ts_start = filename + i + 1;
      break;
    }
  }

  if (ts_start == 0) {
    retval = -1;
    goto done;
  }

  if (sscanf(ts_start, "%ld-1.gwf", timestamp) != 1) {
    retval = -1;
    goto done;
  }

 done:
  return retval;
}

//
// properties
//
enum property {
  ARG_SHM_DIRNAME = 1,
  ARG_MASK,
  ARG_WAIT_TIME,
  ARG_ASSUMED_DURATION
};

static void set_property(GObject *object, guint id, const GValue *value, GParamSpec *pspec)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(object);

  GST_OBJECT_LOCK(element);

  switch((enum property) id) {
  case ARG_SHM_DIRNAME:
    g_free(element->name);
    element->name = g_value_dup_string(value);
    break;

  case ARG_MASK:
    element->mask = g_value_get_uint(value);
    break;

  case ARG_WAIT_TIME:
    element->wait_time = g_value_get_double(value);
    break;
    
  case ARG_ASSUMED_DURATION:
    element->assumed_duration = g_value_get_uint(value);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, id, pspec);
    break;
  }

  GST_OBJECT_UNLOCK(element);
}

static void get_property(GObject *object, guint id, GValue *value, GParamSpec *pspec)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(object);

  GST_OBJECT_LOCK(element);

  switch((enum property) id) {
  case ARG_SHM_DIRNAME:
    g_value_set_string(value, element->name);
    break;

  case ARG_MASK:
    g_value_set_uint(value, element->mask);
    break;

  case ARG_WAIT_TIME:
    g_value_set_double(value, element->wait_time);
    break;

  case ARG_ASSUMED_DURATION:
    g_value_set_uint(value, element->assumed_duration);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, id, pspec);
    break;
  }

  GST_OBJECT_UNLOCK(element);
}

static void finalize(GObject *object)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(object);
  
  g_free(element->name);
  element->name = NULL;
  g_mutex_clear(&element->create_thread_lock);
  
  G_OBJECT_CLASS(gds_devshmsrc_parent_class)->finalize(object);
}


static void gds_devshmsrc_class_init(GDSDEVSHMSrcClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS(klass);
  GstBaseSrcClass *gstbasesrc_class = GST_BASE_SRC_CLASS(klass);
  
  gobject_class->set_property = GST_DEBUG_FUNCPTR(set_property);
  gobject_class->get_property = GST_DEBUG_FUNCPTR(get_property);
  gobject_class->finalize = GST_DEBUG_FUNCPTR(finalize);

  gstbasesrc_class->start = GST_DEBUG_FUNCPTR(start);
  gstbasesrc_class->stop = GST_DEBUG_FUNCPTR(stop);
  gstbasesrc_class->unlock = GST_DEBUG_FUNCPTR(unlock);
  gstbasesrc_class->unlock_stop = GST_DEBUG_FUNCPTR(unlock_stop);
  gstbasesrc_class->create = GST_DEBUG_FUNCPTR(create);
  gstbasesrc_class->query = GST_DEBUG_FUNCPTR(query);

  gst_element_class_set_details_simple(element_class,
                                       "GDS DEVSHM Frame File Source",
                                       "Source",
                                       "LIGO-Virgo /dev/shm frame file source element",
                                       "Ron Tapia <ron.tapia@ligo.org>"
                                       );
  
  gst_element_class_add_pad_template(element_class,
                                     gst_pad_template_new("src",
                                                          GST_PAD_SRC,
                                                          GST_PAD_ALWAYS,
                                                          gst_caps_from_string("application/x-igwd-frame, " "framed = (boolean) true"))
                                     );
  
  g_object_class_install_property(
                                  gobject_class,
                                  ARG_SHM_DIRNAME,
                                  g_param_spec_string("shm-dirname",
                                                      "Name",
                                                      "Shared memory directory name.  Suggestions:  \"/dev/shm/kafka/L1_O3E2ERep1\", \"/dev/shm/kafka/H1_O3E2ERep1\", \"/dev/shm/kafka/V1_O3E2ERep1\".",
                                                      DEFAULT_SHM_DIRNAME,
                                                      (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT))
                                  );

  g_object_class_install_property(gobject_class,
                                  ARG_MASK,
                                  g_param_spec_uint("mask",
                                                    "Trigger mask",
                                                    "Buffers will be received only from producers whose masks' bit-wise logical and's with this value are non-zero.",
                                                    0, G_MAXUINT, DEFAULT_MASK,
                                                    (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT))
                                  );

  g_object_class_install_property(
                                  gobject_class,
                                  ARG_WAIT_TIME,
                                  g_param_spec_double("wait-time",
                                                      "Wait time",
                                                      "Wait time in seconds (<0 = wait indefinitely, 0 = never wait).",
                                                      -G_MAXDOUBLE, G_MAXDOUBLE, DEFAULT_WAIT_TIME,
                                                      (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT))
                                  );

  g_object_class_install_property(
                                  gobject_class,
                                  ARG_ASSUMED_DURATION,
                                  g_param_spec_uint("assumed-duration",
                                                    "Assumed duration",
                                                    "Assume all files span this much time in seconds.",
                                                    1, G_MAXUINT, DEFAULT_ASSUMED_DURATION,
                                                    (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT))
                                  );
}

static void gds_devshmsrc_init(GDSDEVSHMSrc *element)
{
  GstBaseSrc *basesrc = GST_BASE_SRC(element);

  gst_base_src_set_live(basesrc, TRUE);
  gst_base_src_set_format(basesrc, GST_FORMAT_TIME);

  element->name = NULL;
  element->max_latency = element->min_latency = GST_CLOCK_TIME_NONE;
  element->unblocked = FALSE;
  g_mutex_init(&element->create_thread_lock);
  element->partition = NULL;
}
