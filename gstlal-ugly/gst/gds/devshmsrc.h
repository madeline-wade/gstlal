//
// gds devshm (LIGO-Virgo /dev/shm) source element
//
// Copyright (C) Ron Tapia, The Pennsylvania State University 2022
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#ifndef __GDS_DEVSHMSRC_H__
#define __GDS_DEVSHMSRC_H__

#include <pthread.h>
#include <glib.h>
#include <gst/gst.h>
#include <gst/base/gstpushsrc.h>

G_BEGIN_DECLS

#define GDS_DEVSHMSRC_TYPE \
    (gds_devshmsrc_get_type())

#define GDS_DEVSHMSRC(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GDS_DEVSHMSRC_TYPE, GDSDEVSHMSrc))

#define GDS_DEVSHMSRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GDS_DEVSHMSRC_TYPE, GDSDEVSHMSrcClass))

#define GST_IS_GDS_DEVSHMSRC(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GDS_DEVSHMSRC_TYPE))

#define GST_IS_GDS_DEVSHMSRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GDS_DEVSHMSRC_TYPE))

#define DEVSHM_MAX_FILE_EVENTS 350
#define DEVSHM_NEW_FILE_TIMEOUT   350000 // milliseconds before we timeout waiting for a new file

typedef struct {
  GstPushSrcClass parent_class;
} GDSDEVSHMSrcClass;

typedef struct {
  GstPushSrc basesrc;
  
  // Properties
  char  *name;          // should be the full path to a directory under /dev/shm
  double wait_time;        //??
  guint  assumed_duration;  //??
  
  // latency
  GstClockTimeDiff max_latency;
  GstClockTimeDiff min_latency;

  // state
  gboolean     unblocked;
  pthread_t    create_thread;      
  GMutex       create_thread_lock;
  gboolean     need_new_segment; 
  GstClockTime next_timestamp;

  int     notifyFd; // Used for file creation notification.
  char   *inotifyEventp; // Pointer to storage for incoming file creation notofications.
  size_t  inotifyEventLen;
  int     inotifyWatchDescriptor;
  GQueue *filenameQueue; // Used to maintain a list of files to be processed.

  uint          mask;
  int           currentFd;               // File descriptor of the currently opened file.
  char          currentFilename[NAME_MAX + 1]; // Name of the currenlty opened file.
  size_t        currentFilenameLen;
  size_t        currentFileSize;
  GString      *currentFullPath;
  GstClockTime  currentFileTimestamp; // 

  void *partition; // ??
 
} GDSDEVSHMSrc;

GType gds_devshmsrc_get_type(void);

G_END_DECLS

#endif  /* __GDS_DEVSHMSRC_H__ */
