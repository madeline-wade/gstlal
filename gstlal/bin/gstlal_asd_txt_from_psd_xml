#!/usr/bin/env python3
#
# Copyright (C) 2018  Kipp Cannon
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


#
# =============================================================================
#
#                                   Preamble
#
# =============================================================================
#


from optparse import OptionParser

from gstlal.psd import read_psd, write_asd_txt


#
# =============================================================================
#
#                                 Command Line
#
# =============================================================================
#


def parse_command_line():
	parser = OptionParser(
		description = ""
	)
	parser.add_option("--prefix", metavar = "string", default = "psd_", help = "Prepend output files with this string (default = \"psd_\").")
	parser.add_option("-v", "--verbose", action = "store_true", help = "Be verbose (optional).")

	options, filenames = parser.parse_args()

	if len(filenames) != 1:
		raise ValueError("only file PSD file may be given")

	return options, filenames


#
# =============================================================================
#
#                                     Main
#
# =============================================================================
#


#
# parse command line
#


options, filenames = parse_command_line()


#
# load the PSD file iterate over instruments, writing ASD to ascii file
#


for filename in filenames:
	for instrument, psd in read_psd(filename, verbose=options.verbose).items():
		filename = "%s%s.txt" % (options.prefix, instrument)
		write_asd_txt(filename, psd, verbose=options.verbose)
