"""This module contains miscellaneous utilities for working with LAL data structures and api

"""
import os
import pathlib
import tempfile
from typing import Union, Iterable, Optional

from lal.utils import CacheEntry


def create_cache(entries: Iterable[Union[str, pathlib.Path, CacheEntry]], cache_path: Optional[Union[str, pathlib.Path]] = None,
				 use_os_tmpdir: bool = True) -> pathlib.Path:
	"""Create a LAL cache file from an iterable of entries

	Args:
		entries:
			Iterable of either str, Path, or CacheEntry. If str or Path, a CacheEntry will be created using CacheEntry.from_T050017
		cache_path:
		use_os_tmpdir:
			bool, if True use 'TMPDIR' env variable else create a tmp directory using tempfile.TemporaryDirectory. Only applies if
			cache_path argument is None.

	Returns:
		pathlib.Path, the path to the cache file
	"""
	# Coerce entry types
	cache_entries = []
	for entry in entries:
		if isinstance(entry, str):
			entry = pathlib.Path(entry)
		if isinstance(entry, pathlib.Path):
			entry = CacheEntry.from_T050017(url=entry.as_uri())
		cache_entries.append(entry)

	# Set cache path
	if cache_path is None:
		tmpdir = os.getenv('TMPDIR') if use_os_tmpdir else tempfile.TemporaryDirectory()
		cache_path = tempfile.NamedTemporaryFile(suffix='.cache', dir=tmpdir).name
	if isinstance(cache_path, str):
		cache_path = pathlib.Path(cache_path)

	# Write cache file
	with open(cache_path.as_posix(), 'w') as fid:
		for entry in cache_entries:
			fid.write(str(entry))

	return cache_path
