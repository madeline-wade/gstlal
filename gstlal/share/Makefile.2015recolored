## @file Makefile.2015recolored
# Makefile for recoloring data to the 2015 noise curves

# Configuration file name
CONFIG = recolor_config.yml

## Noise curves
LIGODESIGNPSD = early_aligo_asd.txt
VIRGODESIGNPSD = v1_early_asd.txt

# function to extract configuration parameters
yaml = $(shell gstlal_inspiral_config $(CONFIG) $(1))

all : dag

segments.xml.gz:
	gstlal_query_gwosc_segments $(call yaml,start) $(call yaml,stop) $(call yaml,ifos)
	gstlal_segments_trim --trim 0 --gps-start-time $(call yaml,start) --gps-end-time $(call yaml,stop) --min-length 512 --output $@ $@

$(LIGODESIGNPSD) :
	wget https://git.ligo.org/lscsoft/gstlal/raw/master/gstlal/share/$@

$(VIRGODESIGNPSD) :
	wget https://git.ligo.org/lscsoft/gstlal/raw/master/gstlal/share/$@

%recolorpsd.xml.gz : $(LIGODESIGNPSD) 
	gstlal_psd_xml_from_asd_txt --instrument $* --output $@ $(LIGODESIGNPSD)

V1recolorpsd.xml.gz : $(VIRGODESIGNPSD)
	gstlal_psd_xml_from_asd_txt --instrument V1 --output $@ $(VIRGODESIGNPSD)

recolor_psd.xml.gz: H1recolorpsd.xml.gz L1recolorpsd.xml.gz V1recolorpsd.xml.gz
	ligolw_add H1recolorpsd.xml.gz L1recolorpsd.xml.gz V1recolorpsd.xml.gz | gzip > $@

.PHONY: dag
dag : segments.xml.gz recolor_psd.xml.gz
	gstlal_fake_frames_workflow -c $(CONFIG)

clean :
	rm -rf *.sub *.dag* *.sh *reference_psd.xml.gz *segments.xml.gz logs *.xml *.xml.gz
	rm -rf $(LIGODESIGNPSD) $(VIRGODESIGNPSD)
	rm -rf frames reference_psd median_psd
