# Script for setting up a development environment on MacOS for GstLAL

# Clear and notify
clear
echo "Setting GstLAL Development Environment"

# Parse arguments
while getopts n:b: flag; do
  case "${flag}" in
  n) ENV_NAME=${OPTARG} ;;
  esac
done
if [[ -z "${ENV_NAME}" ]]; then
  ENV_NAME="gstlal-dev"
fi
echo "Env name: $ENV_NAME"

# Discover repository paths
echo "Discovering repository root paths"
SCRIPT_PATH="$(
  cd -- "$(dirname "$0")" >/dev/null 2>&1
  pwd -P
)"
CONDA_PATH=$(
  builtin cd $SCRIPT_PATH
  pwd
)
SHARE_PATH=$(
  builtin cd $CONDA_PATH
  cd ..
  pwd
)
GSTLAL_PATH=$(
  builtin cd $SHARE_PATH
  cd ..
  pwd
)
export REPO_PATH=$(
  builtin cd $GSTLAL_PATH
  cd ..
  pwd
)
PARENT_PATH=$(
  builtin cd $REPO_PATH
  cd ..
  pwd
)
#APP_PARENT_PATH=$(
#  builtin cd $PARENT_PATH
#  cd ..
#  pwd
#)
APP_PARENT_PATH=$PARENT_PATH
echo "Repository root discovered: $REPO_PATH"

# Check if deps directory already exists
if [ -d "$APP_PARENT_PATH/deps" ]; then
  echo "Sibling directory already exists, no need to create."
else
  echo "Sibling directory does not exist, creating."
  mkdir "$APP_PARENT_PATH/deps"
fi

# Set dependency-path variable for gstlal build dependencies
if [[ -z "${DEPS_PATH}" ]]; then
  echo "DEPS_PATH variable not set. Setting to: $APP_PARENT_PATH/deps."
  export DEPS_PATH=${APP_PARENT_PATH}/deps
fi

# Add dep-path to PATH variable
echo "DEPS_PATH variable set. Including in PATH"
export PATH=${DEPS_PATH}/bin:${PATH}

# Add deps-path to pkg-config path so pkg-config can tell if gstlal installed
echo "Adding deps to PKG_CONFIG_PATH"
export PKG_CONFIG_PATH=${DEPS_PATH}/lib/pkgconfig:${DEPS_PATH}/lib64/pkgconfig:${PKG_CONFIG_PATH}

# Add deps-path to GStreamer plugin path to Gst can load custom plugins
echo "Adding deps to GST_PLUGIN_PATH"
export GST_PLUGIN_PATH=${DEPS_PATH}/lib/gstreamer-1.0:${GST_PLUGIN_PATH}

# Checking for PYTHONPATH and updating to include deps if needed
echo "Adding deps to PYTHONPATH"
if [[ -z "${PYTHONPATH}" ]]; then
  export PYTHONPATH=${DEPS_PATH}/lib/python3.7/site-packages:${DEPS_PATH}/lib64/python3.7/site-packages
else
  export PYTHONPATH=${DEPS_PATH}/lib/python3.7/site-packages:${DEPS_PATH}/lib64/python3.7/site-packages:$PYTHONPATH
fi
export PYTHONPATH=${DEPS_PATH}/lib/python3.8/site-packages:${DEPS_PATH}/lib64/python3.8/site-packages:$PYTHONPATH
export PYTHONPATH=${DEPS_PATH}/lib/python3.9/site-packages:${DEPS_PATH}/lib64/python3.9/site-packages:$PYTHONPATH

# Additional required variables
echo "Setting additional required GstLAL env variables"
export GSTLAL_FIR_WHITEN=0

# Determine Operating System
echo "Detecting OS"
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  SYS_OS="linux"
  ENV_LOCK_ROOT=$CONDA_PATH/envs/lock/$ENV_NAME-linux-64.lock
elif [[ "$OSTYPE" == "darwin"* ]]; then
  SYS_OS="macos"
  ENV_LOCK_ROOT=$CONDA_PATH/envs/lock/$ENV_NAME-osx-64.lock
elif [[ "$OSTYPE" == "cygwin" ]]; then
  SYS_OS="cygwin"
  echo "Cygwin currently unsupported"
  exit
elif [[ "$OSTYPE" == "msys" ]]; then
  SYS_OS="win"
  echo "Windows currently unsupported"
  exit
else
  echo "Unsupported OS: $OSTYPE"
  exit
fi
echo "OS detected: $SYS_OS"

ENV_LOCK_PATH="$ENV_LOCK_ROOT"

# MacOS specific installs
if [[ ($SYS_OS == "macos") ]]; then
  # Check XCode installed
  # Xcode
  if pkgutil --pkgs=com.apple.pkg.Xcode >/dev/null; then
    echo "Xcode: $(pkgutil --pkg-info=com.apple.pkg.Xcode | awk '/version:/ {print $2}')"
  else
    echo "Xcode not installed, please install XCode, see: https://apps.apple.com/us/app/xcode/id497799835?mt=12"
    exit
  fi

  # Check XCode command line tools installed
  if pkgutil --pkgs=com.apple.pkg.CLTools_Executables >/dev/null; then
    echo "CommandLineTools: $(pkgutil --pkg-info=com.apple.pkg.CLTools_Executables | awk '/version:/ {print $2}')"
  else
    echo "XCode CommandLineTools: not installed. Please install command line tools by running 'xcode-select —install'"
    exit
  fi

  # Check brew is installed
  if ! command -v brew &>/dev/null; then
    echo "Homebrew not installed, unable to install glib without brew. Please install brew by visiting: https://brew.sh"
    exit
  fi

  # The below install is because there is an unresolvable conda conflict on macos between the version of pygobject
  # we need and glib, so we install glib via brew.
  # Check: glib installed via brew
  if [[ $(brew list | grep glib) ]]; then
    echo "glib installed (yay!)"
  else
    echo "glib not installed, please install glib. See install_env_tools.sh for more."
    exit
  fi

  # The below two installs are because there is no conda build available for htcondor on mac. The only
  # option is to install HTCondor via macports and locate the python bindings afterwards
  # Check: MacPorts installed (for HTCondor installation)
  if ! command -v port &>/dev/null; then
    echo "MacPorts not installed, unable to install HTCondor without MacPorts. Please install MacPorts by visiting: https://www.macports.org/install.php"
    exit
  fi

  # Check if htcondor is installed via macports
  if [[ $(port echo requested | grep htcondor) ]]; then
    echo "HTCondor installed (yay!)"
  else
    echo "HTCondor not installed, please install htcondor using macports. See install_env_tools.sh for more."
    exit
  fi
fi

# Check: conda installed
if ! command -v conda &>/dev/null; then
  echo "Conda not installed. Please install miniconda. See install_env_tools.sh for more."
  exit
fi

echo "Sourcing bash and zsh profiles."
if [[ ($SYS_OS == "macos") ]]; then
  source $HOME/.bash_profile
elif [[ ($SYS_OS == "linux") ]]; then
  source $HOME/.bashrc
fi
source $HOME/.zshrc
conda deactivate
conda activate $BASE_ENV

# Check if env is already defined
if [[ -z $(conda env list | grep "^$ENV_NAME ") ]]; then
  echo "Dev env not found, building from lock: $ENV_LOCK_PATH"
  conda create -n $ENV_NAME --file $ENV_LOCK_PATH --force
else
  echo "Dev env already exists. Checking sha256 for consistency."
  _EXISTING_SPEC_FULL=$CONDA_PATH/envs/.existing-specific-full.yml
  _EXISTING_SPEC=$CONDA_PATH/envs/.existing-specific.yml
  _LOCK_SPEC=$CONDA_PATH/envs/.lock-specific.yml
  rm -f $_EXISTING_SPEC $_EXISTING_SPEC_FULL $_LOCK_SPEC
  conda list --name $ENV_NAME --explicit --md5 >$_EXISTING_SPEC_FULL
  awk 'NR>=5' $_EXISTING_SPEC_FULL >$_EXISTING_SPEC
  awk 'NR>=5' $ENV_LOCK_PATH >$_LOCK_SPEC
  EXISTING_ENV_SHA=$(shasum -a 256 $_EXISTING_SPEC | cut -c1-64)
  LOCK_ENV_SHA=$(shasum -a 256 $_LOCK_SPEC | cut -c1-64)
  if [[ "$EXISTING_ENV_SHA" == "$LOCK_ENV_SHA" ]]; then
    echo "SHA match, env is up to date with lock."
  else
    echo "SHA mismatch, env is not up to date with lock. Removing and building."
    conda deactivate
    conda activate $BASE_ENV
    conda env remove -n $ENV_NAME
    conda create -n $ENV_NAME --file $ENV_LOCK_PATH --force
  fi
  rm -f $_EXISTING_SPEC $_EXISTING_SPEC_FULL $_LOCK_SPEC
fi

# Activate conda env
echo "Activating dev env"
conda activate $ENV_NAME
