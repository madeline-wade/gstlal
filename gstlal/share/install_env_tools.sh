# Script for installing necessary environment management tools

# Clear and notify
clear
echo "Installing Development Environment Management Tools"

# Discover repository paths
echo "Discovering repository root paths"
SCRIPT_PATH="$(
  cd -- "$(dirname "$0")" >/dev/null 2>&1
  pwd -P
)"
CONDA_PATH=$(
  builtin cd $SCRIPT_PATH
  pwd
)
SHARE_PATH=$(
  builtin cd $CONDA_PATH
  cd ..
  pwd
)
GSTLAL_PATH=$(
  builtin cd $SHARE_PATH
  cd ..
  pwd
)
export REPO_PATH=$(
  builtin cd $GSTLAL_PATH
  cd ..
  pwd
)
PARENT_PATH=$(
  builtin cd $REPO_PATH
  cd ..
  pwd
)
echo "Repository root discovered: $REPO_PATH"

# Determine Operating System
echo "Detecting OS"
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  SYS_OS="linux"
  ENV_LOCK_ROOT=$CONDA_PATH/envs/qualified-linux-64
elif [[ "$OSTYPE" == "darwin"* ]]; then
  SYS_OS="macos"
  ENV_LOCK_ROOT=$CONDA_PATH/envs/qualified-osx-64
elif [[ "$OSTYPE" == "cygwin" ]]; then
  SYS_OS="cygwin"
  echo "Cygwin currently unsupported"
  exit
elif [[ "$OSTYPE" == "msys" ]]; then
  SYS_OS="win"
  echo "Windows currently unsupported"
  exit
else
  echo "Unsupported OS: $OSTYPE"
  exit
fi
echo "OS detected: $SYS_OS"

# Check if non-vanilla env specified by name
ENV_SUB=$1
if [[ -z $ENV_SUB ]]; then
  echo "No sub env passed"
  ENV_LOCK_PATH="$ENV_LOCK_ROOT.lock"
  ENV_NAME="gstlal-dev-env"
else
  echo "Sub env: $ENV_SUB"
  ENV_LOCK_PATH="$ENV_LOCK_ROOT-$ENV_SUB.lock"
  ENV_NAME="gstlal-dev-env-$ENV_SUB"
fi

# MacOS specific installs
if [[ ($SYS_OS == "macos") ]]; then
  # Check XCode installed
  # Xcode
  if pkgutil --pkgs=com.apple.pkg.Xcode >/dev/null; then
    echo "Xcode: $(pkgutil --pkg-info=com.apple.pkg.Xcode | awk '/version:/ {print $2}')"
  else
    echo "Xcode not installed, please install XCode, see: https://apps.apple.com/us/app/xcode/id497799835?mt=12"
    exit
  fi

  # Check XCode command line tools installed
  if pkgutil --pkgs=com.apple.pkg.CLTools_Executables >/dev/null; then
    echo "CommandLineTools: $(pkgutil --pkg-info=com.apple.pkg.CLTools_Executables | awk '/version:/ {print $2}')"
  else
    echo "XCode CommandLineTools: not installed. Please install command line tools by running 'xcode-select —install'"
    exit
  fi

  # Check brew is installed
  if ! command -v brew &>/dev/null; then
    echo "Homebrew not installed, unable to install glib without brew. Please install brew by visiting: https://brew.sh"
    exit
  fi

  # The below install is because there is an unresolvable conda conflict on macos between the version of pygobject
  # we need and glib, so we install glib via brew.
  # Check: glib installed via brew
  if [[ $(brew list | grep glib) ]]; then
    echo "glib installed (yay!)"
  else
    echo "glib not installed, installing. Please type 'y' when prompted."
    brew install glib
  fi

  # The below two installs are because there is no conda build available for htcondor on mac. The only
  # option is to install HTCondor via macports and locate the python bindings afterwards
  # Check: MacPorts installed (for HTCondor installation)
  if ! command -v port &>/dev/null; then
    echo "MacPorts not installed, unable to install HTCondor without MacPorts. Please install MacPorts by visiting: https://www.macports.org/install.php"
    exit
  fi

  # Check if htcondor is installed via macports
  if [[ $(port echo requested | grep htcondor) ]]; then
    echo "HTCondor installed (yay!)"
  else
    echo "HTCondor not installed, installing. Please type 'y' when prompted."
    sudo port install htcondor
  fi
fi

# Check: conda installed
if ! command -v conda &>/dev/null; then
  echo "Conda not installed. Installing miniconda."

  if [[ ($SYS_OS == "macos") ]]; then
    curl https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh -o ~/miniconda.sh
    bash ~/miniconda.sh -b -p $HOME/miniconda

    # Initialize shells
    echo "Initializing bash"
    $HOME/miniconda/bin/conda init bash
    echo "Initializing zsh"
    $HOME/miniconda/bin/conda init zsh
  elif [[ ($SYS_OS == "linux") ]]; then
    curl https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -o ~/miniconda.sh
    bash ~/miniconda.sh -b -p $HOME/miniconda

    # Initialize shells
    echo "Initializing bash"
    $HOME/miniconda/bin/conda init bash
    echo "Initializing zsh"
    $HOME/miniconda/bin/conda init zsh
  fi

  echo "If this script fails, you will need to close and reopen the shell."
fi
