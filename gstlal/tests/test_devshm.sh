#!/bin/bash
outdir=`mktemp -d ./devshm-out.XXX`
if [ ! -d "$outdir" ]; then
    echo "Could not create temporary directory."
    exit 1
fi
echo "Writing files to directory: $outdir"
gst-launch-1.0 gds_devshmsrc shm-dirname=/dev/shm/kafka/L1_O3ReplayMDC  num-buffers=60 ! framecpp_filesink path=$outdir instrument=L1
