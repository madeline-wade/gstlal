"""Unittests for encoding pipeparts"""
import pytest

from gstlal import gstpipetools
from gstlal.pipeparts import encode, pipetools
from gstlal.pipeparts.pipetools import Gst, GObject
from gstlal.utilities import testtools


def assert_elem_props(elem, name):
	assert gstpipetools.is_element(elem)
	assert type(elem).__name__ == name


class TestEncode:
	"""Group test for encoders"""

	@pytest.fixture(scope='class', autouse=True)
	def pipeline(self):
		GObject.MainLoop()
		return Gst.Pipeline('TestEncode')

	@pytest.fixture(scope='class', autouse=True)
	def src(self, pipeline):
		return pipetools.make_element_with_src(pipeline, None, 'fakesrc')

	def test_wav(self, pipeline, src):
		"""Test wav"""
		elem = encode.wav(pipeline, src)
		assert_elem_props(elem, 'GstWavEnc')

	@testtools.broken('vorbis not available in current version of Gst')
	def test_vorbis(self, pipeline, src):
		"""Test vorbis"""
		elem = encode.vorbis(pipeline, src)
		assert_elem_props(elem, 'GstVorbisEnc')

	@testtools.broken('theora not available in current version of Gst')
	def test_theora(self, pipeline, src):
		"""Test theora"""
		elem = encode.theora(pipeline, src)
		assert_elem_props(elem, 'GstTheoraEnc')

	@testtools.broken('flac not available in current version of Gst')
	def test_flac(self, pipeline, src):
		"""Test flac"""
		elem = encode.flac(pipeline, src)
		assert_elem_props(elem, 'GstFlacEnc')

	@testtools.impl_deprecated
	@testtools.requires_full_build
	def test_igwd_parse(self, pipeline, src):
		"""Test igwd parse"""
		elem = encode.igwd_parse(pipeline, src)
		assert_elem_props(elem, 'GstFrameCPPIGWDParse')

	def test_uri_decode_bin(self, pipeline, src):
		"""Test uri_decode"""
		elem = encode.uri_decode_bin(pipeline, src)
		assert_elem_props(elem, 'GstURIDecodeBin')
