"""Unittests for Streams

"""
import datetime
import inspect
import types
import typing
from unittest import mock

# TODO remove this cruft with new conda-flow set
import os

os.environ['GSTLAL_FIR_WHITEN'] = '0'

from gstlal.pipeparts import condition
from gstlal.stream import Stream

from gstlal import datasource, pipeparts, stream
from gstlal.datasource import DataSource, Detector, KNOWN_LIVE_DATASOURCES


def assert_datasourceinfo_stream_set(stream, info, detectors):
	"""Utility for testing that a Stream instance was correctly set with DataSourceInfo"""
	if isinstance(detectors, Detector):
		assert type(stream.head).__name__ == 'GstAudioConvert'
	else:
		for detector in stream.keys():
			assert type(stream[detector].head).__name__ == 'GstAudioConvert'

	assert stream.source.datasource == info.data_source
	assert stream.source.is_live == (stream.source.datasource in KNOWN_LIVE_DATASOURCES)
	assert stream.source.gps_range == info.seg
	assert not stream.source.state_vector
	assert not stream.source.dq_vector


class TestStreamFromDataSource:
	"""Test class for new DataSourceInfo api"""

	def test_datasource_white_one_ifo(self):
		"""Test Stream.from_datasource with one detector"""
		info = datasource.DataSourceInfo(
			data_source=DataSource.White,
			gps_start_time=1234567,
			gps_end_time=1234568,
			channel_name={Detector.H1: 'CHANNEL'}
		)
		stream = Stream.from_datasource(info, Detector.H1.value)
		assert_datasourceinfo_stream_set(stream, info, Detector.H1)

	def test_datasource_white_two_ifo(self):
		"""Test Stream.from_datasource with two detectors"""
		info = datasource.DataSourceInfo(
			data_source=DataSource.White,
			gps_start_time=1234567,
			gps_end_time=1234568,
			channel_name={Detector.H1: 'CHANNEL', Detector.L1: 'CHANNEL'}
		)
		stream = Stream.from_datasource(info, [Detector.H1.value, Detector.L1.value])
		assert_datasourceinfo_stream_set(stream, info, [Detector.H1, Detector.L1])


def test_stream_dict_access():
	"""Test Stream dict access methods (keys, values, items)"""
	info = datasource.DataSourceInfo(
		data_source=DataSource.White,
		gps_start_time=1234567,
		gps_end_time=1234568,
		channel_name={Detector.H1: 'CHANNEL', Detector.L1: 'CHANNEL'}
	)
	detectors = {Detector.H1.value, Detector.L1.value}
	stream = Stream.from_datasource(info, detectors)

	assert set(stream.keys()) == detectors
	for s in stream.values():
		assert type(s.head).__name__ == 'GstAudioConvert'
	for detector, s in stream.items():
		assert detector in detectors
		assert type(s.head).__name__ == 'GstAudioConvert'


def test_stream_clear():
	"""Test Stream.clear()"""
	info = datasource.DataSourceInfo(
		data_source=DataSource.White,
		gps_start_time=1234567,
		gps_end_time=1234568,
		channel_name={Detector.H1: 'CHANNEL', Detector.L1: 'CHANNEL'}
	)
	detectors = {Detector.H1.value, Detector.L1.value}
	stream = Stream.from_datasource(info, detectors)
	assert set(stream.keys()) == detectors

	cleared = stream.clear()
	assert not cleared.head and isinstance(cleared.head, dict)
